@component('mail::message')
# Hello, {{$user->name}}

Your email has been updated. Please verify your updated email id using the link below.

@component('mail::button', ['url' => route('users.verify', $user->verification_token)])
Verify
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
