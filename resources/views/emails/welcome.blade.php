@component('mail::message')
# Hello, {{$user->name}}

Thank you for connecting with us and creating your account. Please verify your email account using the link below.If you have any doubts contact us at "admin@somedomain.com".

@component('mail::button', ['url' => route('users.verify', $user->verification_token)])
Verify
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
