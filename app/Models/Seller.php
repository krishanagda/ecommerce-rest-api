<?php

namespace App\Models;

use App\Scopes\SellerScopes;
use App\Transformers\SellerTransformer;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Seller extends User
{
    use HasFactory;

    public string $transformer = SellerTransformer::class;

    protected $table = 'users';

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SellerScopes());
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
